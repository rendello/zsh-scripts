#!/bin/zsh

DATE=$(date +%Y-%m-XX)

if [ ! -e $1/Archive/$DATE ] ; then
	mkdir $1/Archive/$DATE -p
fi

# Can't move itself into itself, so this is fine
mv $1/* $1/Archive/$DATE

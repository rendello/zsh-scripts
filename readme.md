# Scripts that I use

## archiver.zsh

Ever have a directory fill up with junk that you'll never clean through? Don't
you wish you could have it all sorted in an Archive directory by month? Well,
this does that. It can be run any time with cron or whatever, it'll only make a
new folder if it's needed.

An example after three months of running `./archiver.zsh ~/Downloads`

```
/home/rendello/Downloads
└── Archive
    ├── 2019-06-XX
    │   └── qr.png
    ├── 2019-07-XX
    │   ├── Image.jpeg
    │   ├── IMG_7986.JPG
    │   ├── IMG_8089.JPG
    │   ├── k.png
    │   ├── N1.pdf
    │   └── openttd-1.9.2-linux-ubuntu-bionic-amd64.deb
    └── 2019-08-XX
        ├── cc-tweaked-1.12.2-1.83.1.jar
        ├── forge-1.12.2-14.23.5.2768-installer.jar
        ├── forge-1.12.2-14.23.5.2768-universal.jar
        ├── go1.12.7.linux-amd64.tar.gz
        ├── Oracle_VM_VirtualBox_Extension_Pack-6.0.10.vbox-extpack
        ├── plexmediaserver_1.16.1.1291-158e5b199_amd64.deb
        ├── TechnicLauncher.jar
        ├── TIS-3D-MC1.12.2-1.5.1.37.jar
        ├── title_files
        │   └── style.css
        ├── title.html
        ├── Untitled document.pdf
        ├── virtualbox-6.0_6.0.10-132072~Ubuntu~xenial_amd64.deb
        └── wall-min.png

3 directories, 36 files
```
